## Inclusão de Metadados em Prescrição Eletrônica com Assinatura PDF via KMS

Este é exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia C# ASP.NET 

Este exemplo apresenta os passos necessários para a geração de assinatura 
  - Passo 1: Montagem e envio da requisição ao HUB
  - Passo 2: Recebimento da resposta com arquivo assinado


### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Este token de acesso pode ser obtido através da documentação disponibilizada em [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário em [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não seja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatória a posse de um certificado digital que identifica o autor do artefato assinado.

Por esse motivo, é necessário possuir algum certificado instalado em seu repositório local.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

| Variável | Descrição | 
| ------ | ------ | 
| INSERT_VALID_ACCESS_TOKEN | Token de acesso (access_token), obtido no [BRyCloud](https://api-assinatura.bry.com.br/api-assinatura-digital#autenticacao-apis). Para uso corporativo, o token deve ser da pessoa jurídica e para uso pessoal, pessoa física. | 
| KMS_CREDENCIAL | Chave de autorização (PIN) do usuário signatário codificada no formato Base64. | 
| KMS_CREDENCIAL_TIPO | PIN, TOKEN ou GOV | 

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Formas de acesso ao Certificado em Nuvem

* *BRy KMS*: Certificado está armazenado na Plataforma de Certificados em Nuvem da BRy Tecnologia. Para este modo de acesso ao certificado o Tipo da Credencial deve ser **PIN** ou **TOKEN**. Para credenciais com tipo **PIN** o Valor da Credencial corresponde a senha de acesso ao certificado em nuvem codificada em Base64, enquanto que para credenciais do tipo **TOKEN** o Valor da Credencial corresponde a um token de autorização do acesso ao certificado. Mais informações sobre o serviço de certificados em nuvem podem ser encontradas em nossa documentação da [API de Certificado em Nuvem](https://api-assinatura.bry.com.br/api-certificado-em-nuvem).

* *GovBR*: Certificado está armazenado na Plataforma de Assinatura Digital gov.br. Para este modo de acesso ao certificado o Tipo da Credencial deve ser **GOVBR** e o Valor da Credencial corresponde ao token de acesso do usuário. É possível verificar um exemplo da geração deste token de acesso em nossa [API Autenticação GOV.BR](https://gitlab.com/brytecnologia-team/integracao/api-autenticacao-token-govbr/react/gestao-credenciais).

### Autenticação, OID's e seus respectivos valores

Para entendimento dos requisitos de Autenticação, recomendamos que leia o artigo [Assinatura PDF Etapa Única](https://api-assinatura.bry.com.br/api-assinatura-digital#assinatura-pdf-etapa-unica).

Os valores correspondentes a cada tipo de OID podem ser encontrados no endereço [API Assinatura Prescrição](https://api-assinatura.bry.com.br/api-assinatura-medica#assinatura-da-prescricao).

### Uso

    - Passo 1: Execute o Build da aplicação utilizando o Microsoft Visual Studio.
    - Passo 2: Certifique-se de que possui a BRy Extension instalada em seu navegador.
    - Passo 3: Execute o exemplo.
