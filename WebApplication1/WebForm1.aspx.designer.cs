﻿//------------------------------------------------------------------------------
// <gerado automaticamente>
//     Esse código foi gerado por uma ferramenta.
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for recriado
// </gerado automaticamente>
//------------------------------------------------------------------------------

namespace WebApplication1
{


    public partial class WebForm1
    {

        /// <summary>
        /// Controle form1.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;

        /// <summary>
        /// Controle Button1.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Button1;

        /// <summary>
        /// Controle ScriptManager1.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.ScriptManager ScriptManager1;

        /// <summary>
        /// Controle UpdatePanel1.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel UpdatePanel1;

        /// <summary>
        /// Controle DropDownTipoDoc.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DropDownTipoDoc;

        /// <summary>
        /// Controle DropDownTipoProfissional.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DropDownTipoProfissional;

        /// <summary>
        /// Controle TextBoxTipo.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TextBoxTipo;

        /// <summary>
        /// Controle TextBoxUF.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TextBoxUF;

        /// <summary>
        /// Controle TextBoxEspecialidade.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TextBoxEspecialidade;

        /// <summary>
        /// Controle TextBoxSaidaInicializar.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TextBoxSaidaInicializar;
    }
}
