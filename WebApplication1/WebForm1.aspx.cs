using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Diagnostics;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        //You can get a valid token at https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes
        //Insert your JWT token INSERT_VALID_ACCESS_TOKEN

        static string INSERT_VALID_ACCESS_TOKEN = ""; //Insira seu Token JWT
        static string KMS_CREDENCIAL_TIPO = "PIN"; //PIN, TOKEN ou GOV
        static string KMS_CREDENCIAL = ""; //Senha do compartimento KMS codificada em base64

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void AssinarKMS_Click(object sender, EventArgs e)
        {
            try
            {
                if (INSERT_VALID_ACCESS_TOKEN == "")
                    throw new Exception("⚠ Please Insert a valid Access Token At Line 22");

                if (this.TextBoxTipo.Text == "" || this.TextBoxUF.Text == "")
                    throw new Exception("⚠ Please, check provived information");
                
                string meta = criaFormularioMetadados();

                //We call the function to initialize
                byte[] responseInitialize = AssinarKMS(meta).Result;

                string path = AppDomain.CurrentDomain.BaseDirectory;
                string fileName = "signature.pdf";
                File.WriteAllBytes(path + fileName, responseInitialize);

                if (responseInitialize == null)
                throw new Exception(responseInitialize + "\\n⚠ Please, check if you have credits available and entered a valid JWT token.");

                //We populate the TextBoxSaidaInitializar with the data returned from the FW-HUB
                this.TextBoxSaidaInicializar.Text = "Assinatura gerada com sucesso no diretório " + path + fileName + ".";
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alerts", "javascript:alert('" + ex.Message + "')", true);
            }
        }

        public async static Task<byte[]> AssinarKMS(string meta)
        {
            var requestContent = new MultipartFormDataContent();

            //Buscar o caminho da aplicação
            string path = AppDomain.CurrentDomain.BaseDirectory;

            //Lendo o documento do disco e convertendo para Streamcontent
            var fileStream = new FileStream(path + "./documento.pdf", FileMode.Open);
            var streamContentDocument = new StreamContent(fileStream);

            //configurando a imagem 
            var imageStream = new FileStream(path + "./imagem.jpg", FileMode.Open);
            var streamContentImage = new StreamContent(imageStream);

            DadosAssinatura dados_assinatura = new DadosAssinatura();

            dados_assinatura.algoritmoHash = "SHA256";
            dados_assinatura.perfil = "ADRB";
            dados_assinatura.signatario = ""; // signatario(cpf) ou uuid

            string dados_assinaturaSerialized = Serialize(dados_assinatura);

            ConfiguracaoImagem configuracao_imagem = new ConfiguracaoImagem();

            configuracao_imagem.altura = 30;
            configuracao_imagem.largura = 100;
            configuracao_imagem.posicao = "INFERIOR_ESQUERDO";
            configuracao_imagem.pagina = "PRIMEIRA";
            configuracao_imagem.proporcaoImagem = 30;

            string configuracao_imagemSerialized = Serialize(configuracao_imagem);

            ConfiguracaoTexto configuracao_texto = new ConfiguracaoTexto();

            configuracao_texto.texto = "Assinado para Teste.";
            configuracao_texto.fonte = "COURIER";
            configuracao_texto.pagina = "PRIMEIRA";
            configuracao_texto.tamanhoFonte = 11;

            string configuracao_textoSerialized = Serialize(configuracao_texto);

            requestContent.Add(new StringContent(dados_assinaturaSerialized), "dados_assinatura");
            requestContent.Add(new StringContent(configuracao_imagemSerialized), "configuracao_imagem");
            requestContent.Add(new StringContent(configuracao_textoSerialized), "configuracao_texto");
            requestContent.Add(new StringContent(meta), "metadados");
            requestContent.Add(streamContentImage, "imagem", "imagem.jpg");
            requestContent.Add(streamContentDocument, "documento", "documento.pdf");

            HttpClient client = new HttpClient();
           
            //client.DefaultRequestHeaders.Add("fw_credencial", credencial);
            client.DefaultRequestHeaders.Add("Authorization", INSERT_VALID_ACCESS_TOKEN);
            client.DefaultRequestHeaders.Add("kms_credencial", KMS_CREDENCIAL);
            client.DefaultRequestHeaders.Add("kms_credencial_tipo", KMS_CREDENCIAL_TIPO);

            //Envia os dados de inicialização da assinatura em formato Multipart
            var response = await client.PostAsync("https://hub2.bry.com.br/fw/v1/pdf/kms/assinaturas/", requestContent).ConfigureAwait(false);


            // O resultado do BRy Framework é transformado em um array de bytes para gravação do arquivo
            byte[] bytes = await response.Content.ReadAsByteArrayAsync().ConfigureAwait(false);
            return bytes;
        }

        public static string Serialize<T>(T obj)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = Encoding.UTF8.GetString(ms.ToArray());
            return retVal;
        }

        public static T Deserialize<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serializer.ReadObject(ms);
            ms.Close();
            return obj;
        }
        
        Metadados metadadosObj;
        string metadadosJs;
        public string criaFormularioMetadados() {
            metadadosObj = new Metadados();

            //Pegando dados inseridos nos campos de metadados e atribuindo OIDs respectivos ao tipo de profissinal
            //Caso seja médico, insere os OIDs vazios do farmacêutico também
            this.metadadosObj = new Metadados
            {
                tipoDocumento = this.DropDownTipoDoc.Text,
                tipoMedico = this.DropDownTipoProfissional.Text,
                numero = this.TextBoxTipo.Text,
                uf = this.TextBoxUF.Text,
                especialidade = this.TextBoxEspecialidade.Text
            };
            if (metadadosObj.tipoMedico == "Medico")
            {
                metadadosObj.numeroOID = "2.16.76.1.4.2.2.1";
                metadadosObj.ufOID = "2.16.76.1.4.2.2.2";
                metadadosObj.especialidadeOID = "2.16.76.1.4.2.2.3";
                metadadosJs = "{\"" + metadadosObj.tipoDocumento + "\"" + ":" + "\"" + "\"" + "," + "\"" + metadadosObj.numeroOID + "\":\"" + metadadosObj.numero + "\",\"" + metadadosObj.ufOID + "\":\"" + metadadosObj.uf + "\",\"" + metadadosObj.especialidadeOID + "\":\"" + metadadosObj.especialidade + "\",\"" + "2.16.76.1.4.2.3.1" + "\":\"" + "" + "\"," + "\"" + "2.16.76.1.4.2.3.2" + "\":\"" + "" + "\"," + "\"" + "2.16.76.1.4.2.3.3" + "\":\"" + "" + "\"" + "}";
            }
            else if (metadadosObj.tipoMedico == "Farmaceutico")
            {
                metadadosObj.numeroOID = "2.16.76.1.4.2.3.1";
                metadadosObj.ufOID = "2.16.76.1.4.2.3.2";
                metadadosObj.especialidadeOID = "2.16.76.1.4.2.3.3";
                metadadosJs = "{\"" + metadadosObj.tipoDocumento + "\"" + ":" + "\"" + "\"" + "," + "\"" + metadadosObj.numeroOID + "\":\"" + metadadosObj.numero + "\",\"" + metadadosObj.ufOID + "\":\"" + metadadosObj.uf + "\",\"" + metadadosObj.especialidadeOID + "\":\"" + metadadosObj.especialidade + "\"}";
            }
            else
            {
                metadadosObj.numeroOID = "2.16.76.1.4.2.12.1";
                metadadosObj.ufOID = "2.16.76.1.4.2.12.2";
                metadadosObj.especialidadeOID = "2.16.76.1.4.2.12.3";
                metadadosJs = "{\"" + metadadosObj.tipoDocumento + "\"" + ":" + "\"" + "\"" + "," + "\"" + metadadosObj.numeroOID + "\":\"" + metadadosObj.numero + "\",\"" + metadadosObj.ufOID + "\":\"" + metadadosObj.uf + "\",\"" + metadadosObj.especialidadeOID + "\":\"" + metadadosObj.especialidade + "\"}";
            }
            return metadadosJs;
        }
    }

    // Data Contracts, utilizados para Serializar e Desserializar JSON Objects

    [DataContract]
    public class DadosAssinatura
    {
        [DataMember]
        public string signatario;

        [DataMember]
        public string algoritmoHash;

        [DataMember]
        public string perfil;
    }

    [DataContract]
    public class ConfiguracaoImagem
    {
        [DataMember]
        public int altura;

        [DataMember]
        public int largura;

        [DataMember]
        public int coordenadaX;

        [DataMember]
        public int coordenadaY;

        [DataMember]
        public string posicao;

        [DataMember]
        public string pagina;

        [DataMember]
        public int proporcaoImagem;
    }

    [DataContract]
    public class ConfiguracaoTexto
    {

        [DataMember]
        public int coordenadaX;

        [DataMember]
        public int coordenadaY;

        [DataMember]
        public string texto;

        [DataMember]
        public string pagina;

        [DataMember]
        public string fonte;

        [DataMember]
        public int tamanhoFonte;
    }

    [DataContract]
    public class Metadados
    {
        [DataMember]
        public string tipoDocumento;

        [DataMember]
        public string tipoMedico;

        [DataMember]
        public string numero;

        [DataMember]
        public string numeroOID;

        [DataMember]
        public string uf;

        [DataMember]
        public string ufOID;

        [DataMember]
        public string especialidade;

        [DataMember]
        public string especialidadeOID;
    }
}
